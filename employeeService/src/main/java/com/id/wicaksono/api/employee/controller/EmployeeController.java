package com.id.wicaksono.api.employee.controller;

import com.id.wicaksono.api.employee.model.Employee;
import com.id.wicaksono.api.employee.repository.EmployeeRepository;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(
        value = "/api/v1"
)
@Api(value = "Employee Resource REST Endpoint", tags = {"Employee"})
public class EmployeeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeRepository repository;

    @RequestMapping(
            value = "/employee/",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Save Employee",
            notes = "Save Employee",
            response = Employee.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Employee.class),
            @ApiResponse(code = 400, message = "Bad request", response = Employee.class)
    })
    public Employee add(
            @ApiParam(required = true, value = "employee")
            @Valid @RequestBody Employee employee
    ) {
        LOGGER.info("Employee add: {}", employee);
        return repository.add(employee);
    }

    @RequestMapping(
            value = "/employee/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get Employee",
            notes = "Get Employee",
            response = Employee.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Employee.class),
            @ApiResponse(code = 400, message = "Bad request", response = Employee.class)
    })
    public Employee findById(
            @ApiParam(required = true, value = "id")
            @PathVariable(value = "id") String id
    ) {
        LOGGER.info("Employee find: id={}", id);
        Long employeeId = Long.parseLong(id);
        return repository.findById(employeeId);
    }

    @RequestMapping(
            value = "/employee/list",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get Employees",
            notes = "Get Employees",
            response = Employee.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Employee.class),
            @ApiResponse(code = 400, message = "Bad request", response = Employee.class)
    })
    public List<Employee> findAll() {
        LOGGER.info("Employee find");
        return repository.findAll();
    }

    @RequestMapping(
            value = "/employee/department/{departmentId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get Employee by department id",
            notes = "Get Employee by department id",
            response = Employee.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Employee.class),
            @ApiResponse(code = 400, message = "Bad request", response = Employee.class)
    })
    public List<Employee> findByDepartment(
            @ApiParam(required = true, value = "departmentId")
            @PathVariable("departmentId") String departmentId
    ) {
        LOGGER.info("Employee find: departmentId={}", departmentId);
        Long id = Long.parseLong(departmentId);
        return repository.findByDepartment(id);
    }

    @RequestMapping(
            value = "/employee/organization/{organizationId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(
            value = "Get Employee by organization id",
            notes = "Get Employee by organization id",
            response = Employee.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = Employee.class),
            @ApiResponse(code = 400, message = "Bad request", response = Employee.class)
    })
    public List<Employee> findByOrganization(
            @ApiParam(required = true, value = "organizationId")
            @PathVariable("organizationId") String organizationId
    ) {
        LOGGER.info("Employee find: organizationId={}", organizationId);
        Long id = Long.parseLong(organizationId);
        return repository.findByOrganization(id);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ResponseEntity handleAllExceptions(RuntimeException e) {
        LOGGER.error("Critical Internal server error.", e);
        return new ResponseEntity(e.getMessage(), null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
