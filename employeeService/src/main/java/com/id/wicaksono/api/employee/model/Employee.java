package com.id.wicaksono.api.employee.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Employee", description = "An employee")
public class Employee {
    @ApiModelProperty(value = "The unique identifier of the given employee", required = true)
    private Long id;

    @ApiModelProperty(value = "The organization unique identifier of the given employee", required = true)
    private Long organizationId;

    @ApiModelProperty(value = "The department unique identifier of the given employee", required = true)
    private Long departmentId;

    @ApiModelProperty(value = "The name of the given employee", required = true)
    private String name;

    @ApiModelProperty(value = "The age of the given employee", required = false)
    private int age;

    @ApiModelProperty(value = "The position of the given employee", required = false)
    private String position;

    public Employee() {

    }

    public Employee(Long organizationId, Long departmentId, String name, int age, String position) {
        this.organizationId = organizationId;
        this.departmentId = departmentId;
        this.name = name;
        this.age = age;
        this.position = position;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", organizationId=" + organizationId + ", departmentId=" + departmentId
                + ", name=" + name + ", position=" + position + "]";
    }

}
