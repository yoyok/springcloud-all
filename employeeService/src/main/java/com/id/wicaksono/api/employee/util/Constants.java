package com.id.wicaksono.api.employee.util;

public interface Constants {
    String API_SERVLET = "api";
    String DOCUMENTATION_URL = "/swagger-ui.html";
}
